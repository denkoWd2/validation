<?php
session_start();

if (isset($_SESSION['error_messages'])) $errors = $_SESSION['error_messages'];

?>

    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
        <style>
            .flex {
                display: flex;
                width: 50%;
                flex-direction: column;
                margin: 15px auto;
            }
        </style>
    </head>
    <body>
    <h1>
        Please register to continue.
    </h1>

    <form action="validate.php" method="POST" autocomplete='off'>
        <div class="flex">
            <label for="username">Username: </label>
            <input id="username" name="username" type="text" >
            <?php if (isset($errors['username'])) :?>
                <ul>
                    <?php foreach ($errors['username'] as $message) :?>
                        <li><?= $message ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

        </div>
        <div class="flex">
            <label for="password">Password: </label>
            <input id="password" name="password" type="password">
            <?php if (isset($errors['password'])) : ?>
                <ul>
                    <?php foreach ($errors['password'] as $message) : ?>
                        <li><?= $message ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif;?>

        </div>
        <div class="flex">
            <button>Register</button>
        </div>
    </form>


    </body>
    </html>

<?php session_destroy(); ?>