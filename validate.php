<?php
session_start();
require_once "./vendor/autoload.php";

use Classes\Validator;


$validator = new Validator();

$inputs = $_POST;
$validator->validate($inputs,
    [
        'password' => 'required|min:5|max:10',
        'username' => "required"
    ]);

if ($validator->isValid()) {
    $_SESSION['error_messages'] = $validator->getMessages();

}

header('Location: /');